//
//  Contact+CoreDataProperties.swift
//  ContactList
//
//  Created by Денис Сусеков on 25.09.2021.
//
//

import Foundation
import CoreData


extension Contact {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Contact> {
        return NSFetchRequest<Contact>(entityName: "Contact")
    }

    @NSManaged public var firstName: String?
    @NSManaged public var lastName: String?
    @NSManaged public var birthDate: Date?
    @NSManaged public var companyName: String?
    @NSManaged public var email: String?
    @NSManaged public var phoneNumber: String?

}

extension Contact : Identifiable {

}
