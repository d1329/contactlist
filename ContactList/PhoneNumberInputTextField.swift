import UIKit

class PhoneNumberInputTextField: UITextField, UITextFieldDelegate {
    
    let phoneMask = "+X (XXX) XXX-XX-XX"

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.delegate = self
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else {
            return false
        }
        let newString = (text as NSString).replacingCharacters(in: range, with: string)
        textField.text = format(phoneNumber: newString, with: phoneMask)
        return false
    }
    
    func format(phoneNumber: String, with mask: String) -> String{
        if (phoneNumber == "+") {
            return phoneNumber
        }
        var numbers = phoneNumber.replacingOccurrences(of: "[^0-9]", with: "", options: .regularExpression)
        if (numbers.count == 1 && numbers.suffix(1) != "7") {
            numbers = "7" + numbers
        }
        var formattedPhoneNumber = ""
        var index = numbers.startIndex
        
        for character in mask where index < numbers.endIndex {
            if character == "X" {
                formattedPhoneNumber.append(numbers[index])
                index = numbers.index(after: index)

            } else {
                formattedPhoneNumber.append(character)
            }
        }
        return formattedPhoneNumber
    }
}
