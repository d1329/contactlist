#  Cписок контактов
## Возможности
- Просмотр списка контактов
- Добавление нового контакта
- Редактирование контакта
- Удаление контакта
- Поиск по имени/фамимлии/эл. почте
- Валидация правильности написания эл. почты
- Маска при вводе номера телефона

## Описание классов 
### Models
Модель контакта. Файлы сгенерированы для работы с CoreData.
### ContactListViewController
Контроллер представления списка контактов. 
### ContactInfoViewController
Контроллер предстваления информации о контакте
### AddOrEditContactViewController
Контроллер представления формы для создания/редактирования контакта.
### ContactController
Взаимодействие, связанное с контактами: создание, редактирование и удаление. Также вспомогательные функции, например, возвращение ФИ.
### PhoneNumberInputTextField
Реализация текстового поля с маской для ввода телефона.
### DateInputTextField
Реализация текстового поля с возможностью ввода даты.
