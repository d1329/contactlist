import CoreData

public class ContactController {
    
    public static func getContacts(in context: NSManagedObjectContext) -> [Contact] {
        do {
            let contacts =  try context.fetch(Contact.fetchRequest()) as! [Contact]
            return contacts
        } catch {
            print("Unexpected error while fetching contacts: \(error).")
        }
        return []
    }
    
    public static func deleteContact(_ contact: Contact, in context: NSManagedObjectContext) {
        context.delete(contact)
        submitChanges(in: context)
    }
    
    public static func createContact(in context: NSManagedObjectContext) -> Contact {
        let contact = Contact(context: context)
        return contact
    }
    
    public static func submitChanges(in context: NSManagedObjectContext) {
        do {
            try context.save()
        } catch {
            print("Unexpected error while saving context: \(error).")
        }
    }
    
    public static func getFullName(contact: Contact) -> String {
        let lastName = contact.lastName ?? ""
        let firstName = contact.firstName ?? ""
        
        if (lastName.isEmpty) {
            return firstName
        }
        
        return "\(lastName) \(firstName)"
    }
    
    
}
