import UIKit

class AddOrEditContactViewController: UIViewController {
    
    private let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    var contactForEdit: Contact?
    
    @IBOutlet var firstName: UITextField!
    @IBOutlet var lastName: UITextField!
    @IBOutlet var birthDate: DateInputTextField!
    @IBOutlet var companyName: UITextField!
    @IBOutlet var email: UITextField!
    @IBOutlet var phoneNumber: UITextField!
    @IBOutlet var emailValidationErrorLabel: UILabel!
    @IBOutlet var doneAddingButton: UIBarButtonItem!
    @IBOutlet var doneEditingButton: UIBarButtonItem!
    @IBOutlet var addContactNavigationBar: UINavigationBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
    }
    
    init?(coder: NSCoder, contactForEdit: Contact) {
        self.contactForEdit = contactForEdit
        super.init(coder: coder)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    
    func createNewContact() {
        let contact = ContactController.createContact(in: self.context)
        fillContactProperties(contact)
        ContactController.submitChanges(in: self.context)
    }
    
    func editContact() {
        fillContactProperties(self.contactForEdit!)
        ContactController.submitChanges(in: self.context)
    }
    
    func fillContactProperties(_ contact: Contact) {
        contact.firstName = self.firstName.text
        contact.lastName = self.lastName.text
        contact.companyName = self.companyName.text
        contact.birthDate = self.birthDate.date
        contact.phoneNumber = self.phoneNumber.text
        contact.email = self.email.text
    }
    
    func updateUI() {
        guard contactForEdit != nil else {
            addContactNavigationBar.isHidden = false
            return
        }
        addContactNavigationBar.isHidden = true
        firstName.text = contactForEdit!.firstName
        lastName.text = contactForEdit!.lastName
        companyName.text = contactForEdit!.companyName
        birthDate.date = contactForEdit!.birthDate
        phoneNumber.text = contactForEdit!.phoneNumber
        email.text = contactForEdit!.email
    }
    
    @IBAction func validateFields(_ sender: Any) {
        var isValid = true
        
        if firstName.text!.isEmpty && lastName.text!.isEmpty {
            isValid = false
        }
        
        if !email.text!.isEmpty && !isValidEmail(email.text!) {
            emailValidationErrorLabel.text = "Адрес эл. почты не соответствует формату"
            emailValidationErrorLabel.isHidden = false
            isValid = false
        } else {
            emailValidationErrorLabel.isHidden = true
        }
        
        doneAddingButton.isEnabled = isValid
        doneEditingButton.isEnabled = isValid
    }
    
    @IBAction func doneAddingButtonTapped(_ sender: Any) {
        validateFields(sender)
        guard doneAddingButton.isEnabled else {
            return
        }
        
        createNewContact()
        self.performSegue(withIdentifier: "unwindToContactList", sender: self)
    }
    
    @IBAction func cancelButtonTapped(_ sender: Any) {
        self.performSegue(withIdentifier: "unwindToContactList", sender: self)
    }
    
    @IBAction func doneEditingButtonTapped(_ sender: Any) {
        validateFields(sender)
        guard doneEditingButton.isEnabled else {
            return
        }
        
        editContact()
        self.performSegue(withIdentifier: "unwindToContactInfo", sender: self)
    }
}
