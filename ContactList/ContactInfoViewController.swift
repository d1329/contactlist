import UIKit

class ContactInfoViewController: UIViewController {
    
    private let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    var contact: Contact!
    @IBOutlet var fullNameLabel: UILabel!
    @IBOutlet var companyNameLabel: UILabel!
    @IBOutlet var companyStackView: UIStackView!
    @IBOutlet var birthDateLabel: UILabel!
    @IBOutlet var birthDateStackView: UIStackView!
    @IBOutlet var phoneNumberLabel: UILabel!
    @IBOutlet var phoneNumberStackView: UIStackView!
    @IBOutlet var emailLabel: UILabel!
    @IBOutlet var emailStackView: UIStackView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
    }
    
    private func updateUI() {
        fullNameLabel.text = ContactController.getFullName(contact: self.contact)
        
        if contact.companyName != nil && !contact.companyName!.isEmpty {
            companyStackView.isHidden = false
            companyNameLabel.text = contact.companyName!
        } else {
            companyStackView.isHidden = true
        }
        
        if contact.birthDate != nil {
            birthDateStackView.isHidden = false
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd.MM.YYYY"
            birthDateLabel.text = dateFormatter.string(from: contact.birthDate!)
        } else {
            birthDateStackView.isHidden = true
        }
        
        if contact.phoneNumber != nil && !contact.phoneNumber!.isEmpty {
            phoneNumberStackView.isHidden = false
            phoneNumberLabel.text = contact.phoneNumber!
        } else {
            phoneNumberStackView.isHidden = true
        }
        
        if contact.email != nil && !contact.email!.isEmpty {
            emailStackView.isHidden = false
            emailLabel.text = contact.email!
        } else {
            emailStackView.isHidden = true
        }
    }
    
    @IBSegueAction func editContact(_ coder: NSCoder) -> AddOrEditContactViewController? {
        return AddOrEditContactViewController(coder: coder, contactForEdit: contact!)
    }
    
    @IBAction func unwindToContactInfo(segue: UIStoryboardSegue) {
        updateUI()
    }
    
    @IBAction func deleteContact(_ sender: Any) {
        ContactController.deleteContact(self.contact, in: context)
        performSegue(withIdentifier: "unwindToContactListFromContactInfo", sender: self)
    }
}
