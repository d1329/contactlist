import UIKit

class DateInputTextField: UITextField {
    
    var datePicker: UIDatePicker = UIDatePicker()
    private var _date: Date?
    var date: Date? {
        get {
            return self._date
        }
        set {
            guard newValue != nil else {
                return
            }
            self._date = newValue
            self.text = formatDate(self._date!)
            self.datePicker.date = self._date!
        }
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setUpDatePicker()
        self.inputView = datePicker
    }
    
    func setUpDatePicker() {
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date()
        datePicker.preferredDatePickerStyle = .wheels
        datePicker.addTarget(self, action: #selector(handleDatePicker), for: .valueChanged)
    }
    
    func formatDate(_ date: Date) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.YYYY"
        return dateFormatter.string(from: self.date!)
    }
    
    @objc func handleDatePicker()
    {
        self.date = datePicker.date
        self.text = formatDate(self.date!)
    }
    
}
