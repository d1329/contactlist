import UIKit

class ContactListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {

    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    private var contacts: [Contact]?
    private var contactsForDisplay: [Contact]?
    @IBOutlet var tableView: UITableView!
    @IBOutlet var searchBar: UISearchBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.contacts = ContactController.getContacts(in: context)
        self.contactsForDisplay = contacts
        setUpTableView()
        setUpSearchBar()
        refreshTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        refreshTableView()
    }
    
    private func setUpTableView() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "ContactCell")
    }
    
    private func setUpSearchBar() {
        self.searchBar.delegate = self
    }
    
    private func refreshTableView()
    {
        self.contactsForDisplay?.sort {
            ContactController.getFullName(contact: $0) < ContactController.getFullName(contact: $1)
        }
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.contactsForDisplay!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "ContactCell")!
        cell.textLabel?.text = ContactController.getFullName(contact: self.contactsForDisplay![indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedContact = contactsForDisplay![indexPath.row]
        
        if let viewController = storyboard?.instantiateViewController(identifier: "ContactInfoViewController") as? ContactInfoViewController {
            viewController.contact = selectedContact
            navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if !searchText.isEmpty {
            self.contactsForDisplay = self.contacts?.filter {
                $0.firstName != nil && $0.firstName!.lowercased().contains(searchText.lowercased()) ||
                $0.lastName != nil && $0.lastName!.lowercased().contains(searchText.lowercased()) ||
                $0.email != nil && $0.email!.lowercased().contains(searchText.lowercased())
            }
        } else {
            self.contactsForDisplay = contacts
        }
        refreshTableView()
    }
    
    @IBAction func unwindToContactList(segue: UIStoryboardSegue) {
        searchBar.text = ""
        self.contacts = ContactController.getContacts(in: context)
        self.contactsForDisplay = contacts
        refreshTableView()
    }
}

